# Summary

**Company**: [Marker Therapeutics, Inc.](https://markertherapeutics.com/)

**Ticker**: NASDAQ:MRKR

**Price**: $ (14/09/2018) / **52w Low**: $ / **52w High**: $

**Market Cap**: $ (14/09/2018)

**Institutional Ownership**: % (Q3 2018)

**Cash**: $M (Q3 2018)

**Debt**: $M (Q3 2018) / **Current Liabilities**: $M (3Q 2018)

**Burn**: $M (2018)

## Catalyst

Ovarian Cancer
 * Interim analysis trigger by end of the second quarter of 2019, with interim data reported by year end.

# Info

The Company reported clinical updates in three Baylor College of Medicine (BCM)-sponsored Phase I/II clinical trials using its MultiTAA T cell therapies,
LAPP and MAPP:

Lymphoma

In the Phase I/II clinical trial in lymphoma, BCM has now treated 15 patients with active disease who have failed an average of four lines of prior therapy. Of
these patients, five patients experienced transient disease stabilization followed by disease progression. Four patients have ongoing stable disease of between
9 to 24+ months following infusion of the MultiTAA-specific T cells, while the remaining six have all had complete and durable responses (4 months to 60+
months), as assessed by PET imaging.

· No relapses observed to date for any patient entering a complete response (CR).
· Patients with active disease who have ongoing complete responses after infusion of MultiTAA cells are now between 1 and 5 years in CR (ongoing).
· Several patients with stable disease show potential for durable disease stabilization, with two patients observed to have stable disease for over 9
months and 24 months, respectively.
· Responses in all six patients who entered a CR were associated with an expansion of infused T cells as well as the induction of antigen spreading.
· None of the treated patients developed cytokine release syndrome, neurotoxicity or any other infusion-related adverse events.
BCM has also treated 17 patients who had developed a CR following their last treatment (adjuvant therapy) for lymphoma, and all but two of these patients
remain in remission (3-42 months post-infusion). Monitoring has continued on all patients previously reported, and none of these patients have yet relapsed
with disease. Average duration of remission for patients with a continuing complete response (CCR) is over 26 months (ongoing), versus 16 months
(ongoing) as of the last patient update.

Multiple Myeloma

Marker Therapeutics also provided an update to the BCM-sponsored Phase I/II clinical trial in multiple myeloma. Results were presented at an oral
presentation at the American Society of Hematology (ASH) 2018 Annual Meeting.

· Ten patients with active disease were treated, including:
o One patient with a CR durable for approximately 29 months before relapse, was subsequently given a second treatment infusion of
MultiTAA T cells, resulting in stable disease for 3 months (ongoing) after the second treatment.
o Two patients achieved partial responses (PR) of between 14 and 22 months (ongoing) as of last follow-up.
o All seven remaining patients experienced stabilization of disease following infusion of MultiTAA cells initially. Three patients developed
transient disease stabilization of between 3-7 months with subsequent progression, and four patients have ongoing stable disease.
· Eight patients were treated in remission, with a median follow up of 21 months. Only one patient has relapsed to date.
· Correlative studies show significant expansion of MultiTAA T cells, as well as significant evidence of epitope spreading with expansion of
endogenous T cells specific for tumor-associated antigens that were not targeted by the MultiTAA product.
· MultiTAA therapy appears to be safe and well-tolerated, with no incidence of cytokine release syndrome, neurotoxicity or any other serious adverse
events related to the therapy.

Acute Lymphoblastic Leukemia
Marker Therapeutics also reported initial results from the BCM-sponsored Phase I clinical trial in acute lymphoblastic leukemia (ALL). In this study, patients
were treated with MultiTAA T cells as a maintenance therapy for patients in CR post-allogeneic stem cell transplant. Leukemic relapse remains the major
cause of treatment failure in hematopoietic stem cell transplant (HSCT) recipients.

· 10 patients have been enrolled and treated in this clinical trial, with eight patients evaluable for response. To date, all but one remains in CR, with
patients ranging from 1 to 22 months in CCR (ongoing). Because of the highly refractory nature of these patients, the length of CCRs and the low
rate of relapse amongst these patients, the Company believes that these early results are promising and may represent meaningful clinical benefit.


Marker Therapeutics also reported key updates from clinical studies of TPIV200, its Folate Receptor Alpha (FRα) peptide cancer vaccine product candidate.

Ovarian Cancer

Marker Therapeutics reported that it had completed enrollment in its Phase II study in ovarian cancer (Study FRV-004), using TPIV200 as a maintenance
therapy for patients in their first remission after surgery and platinum-based chemotherapy. Marker has enrolled, randomized, and treated 120 patients at 17
clinical sites. The study completed enrollment six months faster than anticipated. The Company expects to reach its planned interim analysis trigger of 50
patients who have progressed by the end of the second quarter of 2019, with interim data reported by year end.

· Enrollment of this study was completed over six months ahead of schedule, reflecting ongoing management initiatives to improve and enhance
clinical operations efficiency.
· Marker had previously projected the initiation of its interim analysis to begin in Q4 2018, triggered by the 50
th patient to progress following
treatment. Despite faster than expected enrollment of patients in this study, as of the end of December fewer than 50 patients had progressive disease.
As a result, Marker now expects to reach its planned interim analysis trigger by end of the second quarter of 2019, with interim data reported by year
end.

Triple Negative Breast Cancer

Marker Therapeutics also reported initial findings from its interim analysis of its dose-finding study (Study FRV-002) in patients with triple negative breast
cancer, using TPIV200 as a maintenance therapy for patients in remission following first-line therapy. The four-arm study included low and high dose
TPIV200 with or without cyclophosphamide.

· Of 27 patients evaluated to date for immunogenicity, 26 showed significant immune response to the vaccine treatment. Of 80 patients treated at 11
clinical sites, 11 have shown disease progression to date following treatment with TPIV200.
“These additional clinical results strongly augment our existing, previously disclosed patient dataset. In patients who were treated for active disease in
lymphoma, we continue to see long-lived, ongoing complete responses that are now durable beyond five years and have yet to observe a patient who
achieves a CR subsequently relapse,” said Dr. Richard Kenney, Acting Chief Medical Officer of Marker Therapeutics. “Notably, in the adjuvant lymphoma
patients we have also not seen any additional relapses, with several patients now beyond four years in their continuing complete response. While the median
progression-free survival has not yet been reached in any of these trials, observationally it appears that the time to progression for patients receiving
MultiTAA T cell therapy may compare favorably with results reported in CD19 and BCMA-targeted CAR-T studies in lymphoma and multiple myeloma,
without inducing the toxicities normally associated with gene-modified adoptive cell therapies.”

“Given the highly refractory nature of the patients with acute lymphoblastic leukemia treated, we believe the preliminary results appear to be very promising,
with only one patient having relapsed to date,” continued Dr. Kenney. “These early results may indicate that MultiTAA therapy may be able to drive clinical
benefit for these patients without the need for donor-lymphocyte infusions (DLIs), and the associated risk of graft versus host disease (GvHD). Finally, our
clinical sites have been very supportive of our Phase II vaccine studies in ovarian and breast cancer, and their rapid enrollment is a credit to our Principal
Investigators and clinical investigative sites, as well as our clinical operations team. We are pleased with the progress in building our clinical development
infrastructure and believe we can leverage that experience to drive our upcoming MultiTAA T cell studies efficiently.”